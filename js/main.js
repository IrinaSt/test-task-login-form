const backgroundsList = [
    'img/backgrounds/background.png',
    'img/backgrounds/background1.jpg',
    'img/backgrounds/background2.jpg'
];
const loginBackgroundContainer = document.getElementById('loginBackground');

const getShowedBackgrounds = () => {
    return Cookies.getJSON('backgroundsShowed') || [];
};

const getRandomNumber = (min, max) => {
    let rand = min + Math.random() * (max + 1 - min);
    return Math.floor(rand);
}

const chooseBackgroundsList = () => {
    return backgroundsList.filter(element => !getShowedBackgrounds().includes(element));
};

const chooseBackground = () => {
   if(Cookies.get('background')) {
    loginBackgroundContainer.style.backgroundImage = `url('${Cookies.get('background')}')`;
     return false;
   };
    const backgroundList = chooseBackgroundsList();
    return backgroundList[getRandomNumber(0, backgroundsList.length - 1)];
}

const loadBackground = () => {
  if(!chooseBackground()) {
    return false;
  }
 
    const backgroundsShowed = getShowedBackgrounds();
    const newbackground = chooseBackground();
    backgroundsShowed.push(newbackground);
    console.log(newbackground);
    loginBackgroundContainer.style.backgroundImage = `url('${newbackground}')`;
    Cookies.set('background', newbackground, {
      expires: 0.3
    });
    Cookies.set('backgroundsShowed', backgroundsShowed);
};

loadBackground();

